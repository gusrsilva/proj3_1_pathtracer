#include "bvh.h"

#include "CGL/CGL.h"
#include "static_scene/triangle.h"

#include <iostream>
#include <stack>

using namespace std;

namespace CGL {
    namespace StaticScene {

        BVHAccel::BVHAccel(const std::vector<Primitive *> &_primitives,
                           size_t max_leaf_size) {

            root = construct_bvh(_primitives, max_leaf_size);

        }

        BVHAccel::~BVHAccel() {
            if (root) delete root;
        }

        BBox BVHAccel::get_bbox() const {
            return root->bb;
        }

        void BVHAccel::draw(BVHNode *node, const Color &c) const {
            if (node->isLeaf()) {
                for (Primitive *p : *(node->prims))
                    p->draw(c);
            } else {
                draw(node->l, c);
                draw(node->r, c);
            }
        }

        void BVHAccel::drawOutline(BVHNode *node, const Color &c) const {
            if (node->isLeaf()) {
                for (Primitive *p : *(node->prims))
                    p->drawOutline(c);
            } else {
                drawOutline(node->l, c);
                drawOutline(node->r, c);
            }
        }

        BVHNode *BVHAccel::construct_bvh(const std::vector<Primitive *> &prims, size_t max_leaf_size) {

            // Part 2, Task 1:
            // Construct a BVH from the given vector of primitives and maximum leaf
            // size configuration. The starter code build a BVH aggregate with a
            // single leaf node (which is also the root) that encloses all the
            // primitives.


//            BBox centroid_box, bbox;
//
//            for (Primitive *p : prims) {
//                BBox bb = p->get_bbox();
//                bbox.expand(bb);
//                Vector3D c = bb.centroid();
//                centroid_box.expand(c);
//            }
//
//            BVHNode *node = new BVHNode(bbox);
//
//            node->prims = new vector<Primitive *>(prims);
//            return node;

            /*
             * Compute the bounding box of the primitives in prims in a loop.
             *
             * Initialize a new BVHNode with that bounding box.
             *
             * If there are at most max_leaf_size primitives in the list, this is a leaf node. Allocate a new
             *      Vector<Primitive *> for node's primitive list (initialize this with prims) and return the node.
             *
             * If not, we need to recurse left and right. Pick the axis to recurse on (perhaps the largest dimension of
             *      the bounding box's extent).
             *
             * Calculate the split point you are using on this axis (perhaps the midpoint of the bounding box).
             *
             * Split all primitives in prims into two new vectors based on whether their bounding box's centroid's
             *      coordinate in the chosen axis is less than or greater than the split point. (p->get_bbox().centroid()
             *      is a quick way to get a bounding box centroid for Primitive *p.)
             *
             * Recurse, assigning the left and right children of this node to be two new calls to construct_bvh() with
             *      the two primitive lists you just generated.
             *
             * Return the node.
             */

            BBox bbox;
            for (Primitive *p : prims) {
                BBox bb = p->get_bbox();
                bbox.expand(bb);
            }
            BVHNode *node = new BVHNode(bbox);
            if (prims.size() <= max_leaf_size) {
                node->prims = new vector<Primitive *>(prims);
                return node;
            } else {
                double x(bbox.extent.x), y(bbox.extent.y), z(bbox.extent.z);
                int axis = 0;
                if (y >= x and y >= z) {  // Split along y-axis
                    axis = 1;
                } else if (z >= x and z >= y) {
                    axis = 2;
                }

                double centroids[prims.size()];
                for(int i=0; i < prims.size(); i++) {
                    centroids[i] = prims[i]->get_bbox().centroid()[axis];
                }
                quickSort(centroids, 0, prims.size()-1);
                double splitPoint = centroids[prims.size()/2];

                std::vector<Primitive *> p1, p2;
                for (Primitive *p : prims) {
                    if (p->get_bbox().centroid()[axis] < splitPoint) {
                        p1.push_back(p);
                    } else {
                        p2.push_back(p);
                    }
                }
                if (p1.empty()) {
                    std::size_t const half_size = p2.size() / 2;
                    for (int i = 0; i < half_size; i++) {
                        p1.push_back((Primitive *&&) p2.back());
                        p2.pop_back();
                    }
                } else if (p2.empty()) {
                    std::size_t const half_size = p1.size() / 2;
                    for (int i = 0; i < half_size; i++) {
                        p2.push_back((Primitive *&&) p1.back());
                        p1.pop_back();
                    }
                }


                node->l = construct_bvh(p1, max_leaf_size);
                node->r = construct_bvh(p2, max_leaf_size);

                return node;
            }


        }

        void BVHAccel::quickSort(double arr[], int left, int right) {
            int i = left, j = right;
            double tmp;
            double pivot = arr[(left + right) / 2];

            /* partition */
            while (i <= j) {
                while (arr[i] < pivot)
                    i++;
                while (arr[j] > pivot)
                    j--;
                if (i <= j) {
                    tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                    i++;
                    j--;
                }
            };

            /* recursion */
            if (left < j)
                quickSort(arr, left, j);
            if (i < right)
                quickSort(arr, i, right);
        }


        bool BVHAccel::intersect(const Ray &ray, BVHNode *node) const {
            // Part 2, task 3: replace this.
            // Take note that this function has a short-circuit that the
            // Intersection version cannot, since it returns as soon as it finds
            // a hit, it doesn't actually have to find the closest hit.
            double t0, t1;
            if (!node->bb.intersect(ray, t0, t1)) {
                // Ray does not intersect with bounding box
                return false;
            } else if (node->isLeaf()) {
                // We are at a leaf node, need to check all the primitives
                for (Primitive *p : *(node->prims)) {
                    total_isects++;
                    if (p->intersect(ray))
                        return true;
                }
                return false;
            } else {  // We are at an inner node and need to check the children
                // Use temporary variables to prevent short circuiting?
//                bool wasHitLeft = intersect(ray, node->l);
//                bool wasHitRight = intersect(ray, node->r);
//                return wasHitLeft or wasHitRight;
                return intersect(ray, node->l) || intersect(ray, node->r);
            }

        }

        bool BVHAccel::intersect(const Ray &ray, Intersection *i, BVHNode *node) const {
            // Part 2, task 3: replace this

            double t0, t1;
            if (!node->bb.intersect(ray, t0, t1)) {
                // No intersection, return false
                return false;
            } else if (node->isLeaf()) {
                // We are at a leaf, need to test against all primitives
                // contained in this node
                bool wasIntersection = false;
                for (Primitive *p : *(node->prims)) {
                    total_isects++;
                    Intersection temp_i = Intersection();
                    if (p->intersect(ray, &temp_i) and temp_i.t < i->t) {
                        i->t = temp_i.t;
                        i->n = temp_i.n;
                        i->bsdf = temp_i.bsdf;
                        i->primitive = temp_i.primitive;
                        wasIntersection = true;
                    }
                }
                return wasIntersection;
            } else {
                // We are at an inner node and need to check the children nodes
                Intersection hit1 = Intersection();
                Intersection hit2 = Intersection();

                // Use temporary variables to prevent short circuiting
                bool wasHit1 = intersect(ray, &hit1, node->l);
                bool wasHit2 = intersect(ray, &hit2, node->r);
                bool intersection = wasHit1 or wasHit2;

                if (!intersection) {
                    return false;
                } else if (hit1.t < hit2.t) {
                    i->t = hit1.t;
                    i->n = hit1.n;
                    i->bsdf = hit1.bsdf;
                    i->primitive = hit1.primitive;
                    return true;
                } else {
                    i->t = hit2.t;
                    i->n = hit2.n;
                    i->bsdf = hit2.bsdf;
                    i->primitive = hit2.primitive;
                    return true;
                }
            }
        }

    }  // namespace StaticScene
}  // namespace CGL
