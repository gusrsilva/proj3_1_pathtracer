#include "sphere.h"

#include <cmath>

#include  "../bsdf.h"
#include "../misc/sphere_drawing.h"

namespace CGL {
    namespace StaticScene {

        bool Sphere::test(const Ray &r, double &t1, double &t2) const {

            // Part 1, Task 4:
            // Implement ray - sphere intersection test.
            // Return true if there are intersections and writing the
            // smaller of the two intersection times in t1 and the larger in t2.
            double a = dot(r.d, r.d);
            double b = dot(2*(r.o - this->o), r.d);
            double c = dot((r.o - this->o), (r.o-this->o)) - this->r2;

            double disc = b*b - 4*a*c;
            if(disc < 0) {
                return false;
            }

            double temp_t1 = (-b - sqrt(disc))/(2*a);
            double temp_t2 = (-b + sqrt(disc))/(2*a);

            t1 = min(temp_t1, temp_t2);
            t2 = max(temp_t1, temp_t2);

            return (t1 >= r.min_t and t1 <= r.max_t) or (t2 >= r.min_t and t2 <= r.max_t);
        }

        bool Sphere::intersect(const Ray &r) const {

            // Part 1, Task 4:
            // Implement ray - sphere intersection.
            // Note that you might want to use the the Sphere::test helper here.
            double t1, t2;
            bool intersection = test(r, t1, t2);
            if(intersection) {
                if(t1 >= r.min_t) {
                    r.max_t = t1;
                }
                else {
                    r.max_t = t2;
                }
            }
            return intersection;
        }

        bool Sphere::intersect(const Ray &r, Intersection *i) const {

            // Part 1, Task 4:
            // Implement ray - sphere intersection.
            // Note again that you might want to use the the Sphere::test helper here.
            // When an intersection takes place, the Intersection data should be updated
            // correspondingly.
            double t1, t2;
            bool intersection = test(r, t1, t2);

            if(intersection) {
                double t;
                if(t1 >= r.min_t) {
                     t = t1;
                }
                else {
                    t = t2;
                }

                i->t = t;
                i->n = (r.o + t*r.d) - this->o;
                i->n.normalize();
                i->primitive = this;
                i->bsdf = get_bsdf();
                r.max_t = t;


            }
            return intersection;

        }

        void Sphere::draw(const Color &c) const {
            Misc::draw_sphere_opengl(o, r, c);
        }

        void Sphere::drawOutline(const Color &c) const {
            //Misc::draw_sphere_opengl(o, r, c);
        }


    } // namespace StaticScene
} // namespace CGL
