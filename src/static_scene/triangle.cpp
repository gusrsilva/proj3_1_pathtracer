#include "triangle.h"

#include "CGL/CGL.h"
#include "GL/glew.h"

namespace CGL {
    namespace StaticScene {

        Triangle::Triangle(const Mesh *mesh, size_t v1, size_t v2, size_t v3) :
                mesh(mesh), v1(v1), v2(v2), v3(v3) {}

        BBox Triangle::get_bbox() const {

            Vector3D p1(mesh->positions[v1]), p2(mesh->positions[v2]), p3(mesh->positions[v3]);
            BBox bb(p1);
            bb.expand(p2);
            bb.expand(p3);
            return bb;

        }

        bool Triangle::intersect(const Ray &r) const {

            // Part 1, Task 3: implement ray-triangle intersection
            Vector3D p1(mesh->positions[v1]), p2(mesh->positions[v2]), p3(mesh->positions[v3]);
            Vector3D x = mollerTrumbore(p1, p2, p3, r.o, r.d);
            double t(x.x), b1(x.y), b2(x.z), b3(1-b1-b2);

            if(t < r.min_t or t > r.max_t) {
                return false;
            }

            bool intersection = b1 >= 0 and b2 >= 0 and b3 >= 0;
            if(intersection) {
                r.max_t =t;
            }
            return intersection;
        }

        bool Triangle::intersect(const Ray &r, Intersection *isect) const {

            // Part 1, Task 3:
            // implement ray-triangle intersection. When an intersection takes
            // place, the Intersection data should be updated accordingly
            Vector3D p1(mesh->positions[v1]), p2(mesh->positions[v2]), p3(mesh->positions[v3]);
            Vector3D n1(mesh->normals[v1]), n2(mesh->normals[v2]), n3(mesh->normals[v3]);

            /*
             * t is the ray's tt-value at the hit point.
             * n is the surface normal at the hit point. Use barycentric coordinates to interpolate between n1, n2, n3, the per-vertex mesh normals.
             * primitive points to the primitive that was hit (use the this pointer).
             * bsdf points to the surface bsdf at the hit point (use get_bsdf()).
             */
            Vector3D x = mollerTrumbore(p1, p2, p3, r.o, r.d);
            double t(x.x), b2(x.y), b3(x.z), b1(1-b3-b2);

            if(t < r.min_t or t > r.max_t) {
                return false;
            }
            bool intersection  = b1 >= 0 and b2 >= 0 and b3 >= 0;

            if(intersection) {
                isect->t = t;
                isect->n = n1 * b1 + n2 * b2 + n3 * b3;
                isect->primitive = this;
                isect->bsdf = get_bsdf();
                r.max_t = t;
            }

            return intersection;
        }

        Vector3D Triangle::mollerTrumbore(Vector3D p0, Vector3D p1, Vector3D p2, Vector3D o, Vector3D d) const {
            Vector3D e1(p1-p0), e2(p2-p0), s(o-p0), s1(cross(d,e2)), s2(cross(s,e1));

            // x = (1/s1•e1)*[s2•e2, s1•s, s2•d]^T
            return (1.0 / dot(s1,e1))*Vector3D(dot(s2, e2), dot(s1,s), dot(s2,d));
        }

        void Triangle::draw(const Color &c) const {
            glColor4f(c.r, c.g, c.b, c.a);
            glBegin(GL_TRIANGLES);
            glVertex3d(mesh->positions[v1].x,
                       mesh->positions[v1].y,
                       mesh->positions[v1].z);
            glVertex3d(mesh->positions[v2].x,
                       mesh->positions[v2].y,
                       mesh->positions[v2].z);
            glVertex3d(mesh->positions[v3].x,
                       mesh->positions[v3].y,
                       mesh->positions[v3].z);
            glEnd();
        }

        void Triangle::drawOutline(const Color &c) const {
            glColor4f(c.r, c.g, c.b, c.a);
            glBegin(GL_LINE_LOOP);
            glVertex3d(mesh->positions[v1].x,
                       mesh->positions[v1].y,
                       mesh->positions[v1].z);
            glVertex3d(mesh->positions[v2].x,
                       mesh->positions[v2].y,
                       mesh->positions[v2].z);
            glVertex3d(mesh->positions[v3].x,
                       mesh->positions[v3].y,
                       mesh->positions[v3].z);
            glEnd();
        }


    } // namespace StaticScene
} // namespace CGL
