#include "bbox.h"

#include "GL/glew.h"

#include <algorithm>
#include <iostream>

namespace CGL {

    bool BBox::intersect(const Ray &r, double &t0, double &t1) const {

        // Part 2, Task 2:
        // Implement ray - bounding box intersection test
        // If the ray intersected the bounding box within the range given by
        // t0, t1, update t0 and t1 with the new intersection times.
        double tx1 = (this->min.x - r.o.x)/ r.d.x;
        double ty1 = (this->min.y - r.o.y)/ r.d.y;
        double tz1 = (this->min.z - r.o.z)/ r.d.z;

        double tx2 = (this->max.x - r.o.x)/ r.d.x;
        double ty2 = (this->max.y - r.o.y)/ r.d.y;
        double tz2 = (this->max.z - r.o.z)/ r.d.z;

        double tminx(std::min(tx1, tx2)), tminy(std::min(ty1, ty2)), tminz(std::min(tz1, tz2));
        double tmaxx(std::max(tx1, tx2)), tmaxy(std::max(ty1, ty2)), tmaxz(std::max(tz1, tz2));

        double tmin = std::max(std::max(tminx, tminy), tminz);
        double tmax = std::min(std::min(tmaxx, tmaxy), tmaxz);

        if(tmin > tmax) {
            return false;
        }

        t0 = tmin;
        t1 = tmax;

        return true;
    }

    void BBox::draw(Color c) const {

        glColor4f(c.r, c.g, c.b, c.a);

        // top
        glBegin(GL_LINE_STRIP);
        glVertex3d(max.x, max.y, max.z);
        glVertex3d(max.x, max.y, min.z);
        glVertex3d(min.x, max.y, min.z);
        glVertex3d(min.x, max.y, max.z);
        glVertex3d(max.x, max.y, max.z);
        glEnd();

        // bottom
        glBegin(GL_LINE_STRIP);
        glVertex3d(min.x, min.y, min.z);
        glVertex3d(min.x, min.y, max.z);
        glVertex3d(max.x, min.y, max.z);
        glVertex3d(max.x, min.y, min.z);
        glVertex3d(min.x, min.y, min.z);
        glEnd();

        // side
        glBegin(GL_LINES);
        glVertex3d(max.x, max.y, max.z);
        glVertex3d(max.x, min.y, max.z);
        glVertex3d(max.x, max.y, min.z);
        glVertex3d(max.x, min.y, min.z);
        glVertex3d(min.x, max.y, min.z);
        glVertex3d(min.x, min.y, min.z);
        glVertex3d(min.x, max.y, max.z);
        glVertex3d(min.x, min.y, max.z);
        glEnd();

    }

    std::ostream &operator<<(std::ostream &os, const BBox &b) {
        return os << "BBOX(" << b.min << ", " << b.max << ")";
    }

} // namespace CGL
