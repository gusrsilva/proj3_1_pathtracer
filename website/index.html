<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<style>
  body {
    padding: 100px;
    width: auto;
    margin: auto;
    text-align: left;
    font-weight: 300;
    font-family: 'Open Sans', sans-serif;
    color: #121212;
  }
  h1, h2, h3, h3, h4, h5 {
    font-family: 'Source Sans Pro', sans-serif;
  }
</style>
    <title>Gustavo Silva | CS 184</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
    <h1 align="middle">Assignment 3-1: PathTracer</h1>
    <h2 align="middle">Gustavo Silva</h2>

    <div style="padding">

      <p>
        In this project we explore a whole new way of rendering scenes known as
        ray tracing. In order to work our way up to a functional path tracer that
        can render high quality scenes, we implement and explore key ideas such as
        ray generation, ray intersection, BVH construction, accelerating intersection
        tests using the BVH, BSDFs, direct and indirect illumination, monte carlo estimation,
        and adaptive sampling. This project proved trickier than most and required
        the ability to debug though analyzing rendered results and determining
        which phases of the pipeline may be contributing to errors. In the end,
        we were able to render beautiful, detailed, and semi-realistic looking
        scenes of lambertian objects.
      </p>

      <!-- ############################################################### -->
       <!-- ########################### PART 1 ########################### -->
       <!-- ############################################################### -->
      <h2 align="middle">Part 1: Ray Generation and Intersection</h2>
      <h3 align="middle">Ray Generation</h3>
      <p>
        Let's first discuss the process of ray generation. In this part we are
        trying to generate rays from the camera, into the scene. The idea is best
        visualized with the following image from lecture:
          <div align="middle">
              <table style="width=100%">
                  <tr>
                      <td align="middle">
                          <img src="images/p1/generate_ray_vis.png" width="400px" />
                  </tr>
              </table>
          </div>
        We would like to generate the viewing ray in the above image for each
        pixel position in the viewing window. We start by defining our viewing
        window as the plane segment having bottom left and top right corners at:
          <p align="middle"><pre align="middle">
            bottomLeft = (-tan(radians(hFov)*.5), -tan(radians(vFov)*.5), -1)
          </pre></p>
          <p align="middle"><pre align="middle">
            topRight = (tan(radians(hFov)*.5), tan(radians(vFov)*.5), -1)
          </pre></p>
        Thus, in order to generate a ray, we request the position in the x and y
        directions as the ratios of the position to the widths and heights,
        respectively, of the viewing windown (i.e the
        center of the viewing window has x = 0.5, y = 0.5).
        Now in order to calculate the actual position, we simply interpolate
        between the corners that we have already defined. Thus, to get the pixel
         position on the viewing window we have:
         <p align="middle">
           <pre align="middle">
            pixel-position[x] = bottomLeft[x] + x*(topRight[x] - bottomLeft[x])
          </pre>
          <pre align="middle">
           pixel-position[y] = bottomLeft[y] + y*(topRight[y] - bottomLeft[y])
         </pre>
        </p>
        <br>

        Now, recall that a ray has the form:
          <p align="middle"><pre align="middle">r(t) = O + tD</pre></p>
        where O is the origin and D is the direction. As we can see from the
        illustration above, our origin for the viewing ray is the viewing point
        and the direction is the direction of the pixel position. Thus, we create
        our ray by letting the origin be the position of the camera and D be
        the normalized direction of the pixel position, which we calculated above.
        One small technicaility that we had to mind was that our resulting D was
        in camera coordinates and we needed it in world coordinates. We did this
        by multiplying it by our transformation matrix c2w which does just this.
      </p>

      <h3 align="middle">Intersection</h3>
      <p>
        Let us now discuss our triangle intersection algorithm. We wanted to find
        the point t, at which a ray intersects with a triangle. Recall from our
        previous uses of barycentric coordinates that a point P within a
        triangle defined by the points P0, P1, and P2 can be expressed as:
        <p align="middle">
          <pre align="middle">
           P = (1-b1-b2)*P0 + b1*P1 + b2*P2
         </pre>
       </p>
       Thus, if we want to know the t for which a ray intersects a triangle, we
       simply set their equations equal,
       with t, b1, and b2 being the unknowns. We see that what we have is a system of
       equations. We then use a nice little trick known as Cramer's rule (the
       details with which I will not bore you with) to derive a closed form for a
       solution to this system of equations. The closed form expression derived from
       this gives rise to the Moller-Trumbore algorithm, which is what we use to
       find the intersection point of a triangle. It is as follows:
       <div align="center">
           <table style="width=100%">
               <tr>
                   <td align="middle">
                       <img src="images/p1/moller-trumbore.png" width="400px" />
               </tr>
           </table>
       </div>
        Once we have our solution, we know that the ray has intersected the triangle
        if t > 0, meaning the intersection point is not behind the origin of the
        ray, and b1,b2, and 1 - b1 - b2 are all between 0 and 1, meaning the
        intersection point actually lies inside of the triangle.
      </p>
      <h3 align="middle">Examples</h3>
      <p>
        Here are a few examples of a few small dae files using normal shading:
      </p>
      <div align="center">
          <table style="width=100%">
              <tr>
                  <td align="middle">
                      <img src="images/p1/empty.png" height="360px" />
                  </td>
                  <td align="middle">
                      <img src="images/p1/spheres.png" height="360px" />
                  </td>
                  <td align="middle">
                      <img src="images/p1/gems.png" height="360px" />
                  </td>
              </tr>
          </table>
      </div>


      <!-- ############################################################### -->
       <!-- ########################### PART 2 ########################### -->
       <!-- ############################################################### -->
      <h2 align="middle">Part 2: Bounding Volume Heirarchy</h2>

      <h3 align="middle">BVH Construction</h3>
      <p>
        As we all know, BVHs greatly speed up our rendering due to the fact that
        they help us drastically reduce the number of intersection tests we must
        perform by providing us with a structure to perform what I think of as
        a "coarse to fine" search combined with a binary search. There are several
        ways to construct a BVH and I will walk through how I constructed mine.
        Being that the BVH is essentially a tree, a very recursive structure, the
        construction is done in a recursive manner, with a recursive function that
        takes in a list of primitives and a number representing the maximum amount
        of primitives that a leaf should hold. I then did the following at each
        step:
          <ol>
            <li>
              Compute the bounding box of all primitives.
            </li>
            <li>
              Initialize a new BVHNode with this bounding box.
            </li>
            <li>
              If the number of primitives was less than or equal to the total
              that could be held in a leaf node, then this is a leaf node. We
              set the nodes list of primitives to be the current on and
              return this node.
            </li>
            <li>
              Otherwise, this is an inner node. We split the primitives by their
              median point in the largest axis of the bounding box and
              recursively call constructBVH with the two separate lists to get
              the left and right children of this node.
            </li>
          </ol>
        The way I calculated the median was by sorting all the centroids of the
        bounding boxes of the primitives by their value in the axis we are splitting
        on. We then know that the median point in this axis is the middle value of
        this sorted list.
      </p>

      <h3 align="middle">BVH Intersection</h3>
      <p>
        Once we have constructed the BVH, testing intersection is quite straight
        forward. We simply make our way down the tree, recursively, testing
        intersection with the bounding box of each node first. We know that only
        if the ray intersects with the bounding box of the node, should we check
        if it intersects with the children. We do this until we reach a root node,
        at which we must check each primitive for intersection. A small technicality
        arises in the implementation if we would like to know the details about the
        intersection point. This is because we then must check all intersections
        so that we can return the closest intersection point. This prevents us from
        being able to short circuit once we find any intersection and returning true.
        All this meant was that we had two intersection tests, one faster version
        that short circuits and only tells you whether or not there was an
        intersection, and one that checks all intersections and returns the details
        of the closest one.
      </p>


      <h3 align="middle">Examples</h3>
      <p>
        Here are a few examples of some larger dae files using normal shading:
      </p>
      <div align="center">
          <table style="width=100%">
              <tr>
                  <td align="middle">
                      <img src="images/p2/lucy.png" height="360px" />
                  </td>
                  <td align="middle">
                      <img src="images/p2/dragon.png" height="360px" />
                  </td>
                  <td align="middle">
                      <img src="images/p2/max.png" height="360px" />
                  </td>
              </tr>
          </table>
      </div>



      <!-- ############################################################### -->
       <!-- ########################### PART 3 ########################### -->
       <!-- ############################################################### -->
      <h2 align="middle">Part 3: Direct Illumination</h2>

      <h3 align="middle">Implementation</h3>

      <p>
        Direct lighting is lighting the scene with, as the name implies, only
        light directly from light sources. In other words, we do not consider
        secondary light sources that result from light bouncing around the
        scene. On a high level, we do this by summing the estimated
        average of the outgoing radiance of every light in the scene. More
        specifically we:
          <ul>
              <li>
                First loop over all lights in the scene. For each light:
                  <ul>
                    <li>
                      For the appropriate number of samples:
                      <ul>
                          <li>
                            Sample the light for the incoming radiance, hit point,
                            direction from hit point to light source, and pdf.
                          </li>
                          <li>
                            Cast a "shadow ray" from the hit point back to the light
                            to check if it is occluded, by using the BVH to see
                            if an intersection occurs before the ray gets back
                            to the light. This lets us know if this point should
                            be in shade or not.
                          </li>
                          <li>
                            If the point should not be in shade (there was no
                            intersection for the shadow ray), then we need to add
                            the unbiased radiance to an ongoing sum we have for
                            this light. We calculate the unbiased radiance as:
                            <pre>
                             Lr += bsdf(wout, win)*Li*w_in.z /pdf
                           </pre>
                           Li is the incoming radiance sampled from the
                           light. In order to convert this to radiance we must
                           multiply by the cos(incoming angle, normal) which is
                           just dot(w_in, normal) but the way we constructed the
                           coordinate system, the normal is just the standard
                           unit vector in the z direction and so this is equal
                           to the z coordinate of w_in. We then divide by the
                           pdf so that our estimate is unbiased.
                          </li>
                      </ul>
                    </li>
                    <li>
                      We now need to average the accumalated unbiased radiance
                      from this light by dividing by the number of samples used
                      for this light and add it to our running sum of each lights
                      average radiance.
                    </li>
                  </ul>
                  <li>
                    Once we have the sum of all of the averaged radiances from each
                     light we are done.
                  </li>
              </li>
          </ul>
      </p>
      <h3 align="middle">Examples</h3>
      <p>
        Here are a few examples of some images rendered using direct illumination:
      </p>
      <div align="center">
          <table style="width=100%">
              <tr>
                  <td align="middle">
                    <img src="images/p3/bunny_32_64.png" height="360px" />
                  </td>
                  <td align="middle">
                    <img src="images/p3/dragon_32_64.png" height="360px" />
                  </td>
                  <td align="middle">
                      <img src="images/p3/max_32_64.png" height="360px" />
                  </td>
              </tr>
          </table>
      </div>
      <p>
        The above images were generated using 32 light rays and 64 samples per
        pixel which is quite expensive. Let us examine the first image using
        different settings.
      </p>
      <div align="center">
          <table style="width=100%">
              <tr>
                  <td align="middle">
                      <img src="images/p3/bunny_1_1.png" height="360px" />
                      <figcaption align="middle">Light Rays: 1 Samples: 1</figcaption>
                  </td>
                  <td align="middle">
                    <img src="images/p3/bunny_4_1.png" height="360px" />
                    <figcaption align="middle">Light Rays: 4 Samples: 1</figcaption>
                  </td>
              </tr>
              <tr>
                  <td align="middle">
                    <img src="images/p3/bunny_16_1.png" height="360px" />
                    <figcaption align="middle">Light Rays: 16 Samples: 1</figcaption>
                  </td>
                  <td align="middle">
                    <img src="images/p3/bunny_64_1.png" height="360px" />
                    <figcaption align="middle">Light Rays: 64 Samples: 1</figcaption>
                  </td>
              </tr>
          </table>
      </div>
      <p>
        We can see that as the number of light rays increases, the amount of noise
        overall, but especially in areas of soft shadows, dramatically decreases.
      </p>


      <!-- ############################################################### -->
       <!-- ########################### PART 4 ########################### -->
       <!-- ############################################################### -->
      <h2 align="middle">Part 4: Indirect Illumination</h2>

      <h3 align="middle">Implementation</h3>
      <p>
        Indirect lighting differs from direct lighting in the fact that we now
        also use light from secondary and further light sources (light that
        bounces off of an object instead of coming directly from a light source),
        in order to estimate the lighting at each pixel. We do this in a recursive
        manner, where at each recursive call we perform the following steps:
          <ol>
              <li>
                Use the outgoing radiance to sample the surface BSDF at the
                intersection point for the incoming radiance, an incomning ray
                direction w_in, and the value of the sampling pdf at this
                direction.
              </li>
              <li>
                We then use the illuminance of the sampled spectrum to derive a
                directly related probability p, known as the russian roulette
                termination probablilty, which represents the probability
                we will terminate (stop recursing).
              </li>
              <li>
                With probability p:
              </li>
                <ul>
                  <li>
                    Create a ray that starts a very small distance from the hit
                    point and travels in the direction (in world coordinates) of
                    w_in. Each ray has a depth attribute that effectively
                    holds the maximum number of bounces this ray can go before
                    terminating. We set the depth of our new ray as 1 less than
                    the current ray.
                  </li>
                  <li>
                    We then estimate the incoming radiance of this ray by tracing
                    it. This is where the recursion happens since each time we trace
                    a ray we again calculate the indirect lighting.
                  </li>
                  <li>
                    Similar to direct lighting, we then convert this incoming
                    radiance into an outgoing radiance estimator by scaling by
                    the BSDF and a cosine factor and dividing by the BSDF pdf. We
                    now must also, however, divide by one minus the Russian
                    roulette termination probability to remain unbiased. We
                    then return this value as our final estimation.
                  </li>
                </ul>
          </ol>
          <h3 align="middle">Examples</h3>
          <p>
            When we combine direct and indirect illumination, we have global
            illumination. Here are some examples
          </p>
          <div align="center">
              <table style="width=100%">
                  <tr>
                      <td align="middle">
                          <img src="images/p4/global_spheres.png" height="360px" />
                          <figcaption align="middle">Light Rays: 32 Samples: 64 Max Ray Depth: 6</figcaption>
                      </td>
                      <td align="middle">
                        <img src="images/p4/global_bunny.png" height="360px" />
                        <figcaption align="middle">Light Rays: 32 Samples: 64 Max Ray Depth: 6</figcaption>
                      </td>
                  </tr>
              </table>
          </div>
          <h3 align="middle">Comparisons</h3>

          <h4 align="middle">Indirect vs Direct</h4>
          <p>
            Let us now see how each illumination type (direct and indirect)
            contributes to the overall global illumination. Below is the spheres
            scene from above rendered using only direct illumination and rendered
            using only indirect illumination.
          </p>
          <div align="center">
              <table style="width=100%">
                  <tr>
                      <td align="middle">
                          <img src="images/p4/direct_spheres.png" height="360px" />
                          <figcaption align="middle">Only Direct Illumination</figcaption>
                      </td>
                      <td align="middle">
                        <img src="images/p4/indirect_spheres.png" height="360px" />
                        <figcaption align="middle">Only Indirect Illumination</figcaption>
                      </td>
                  </tr>
              </table>
          </div>
          <p>
            As is expected the scene lit with direct light is much brighter than
            the scene lit with only inderect lighting. There is more going on here,
            though. Notice first that in the direct scene, the bottom half of the
            spheres are almost completely black. This is because we are not accounting
            for the light that reflects off of the floor that would normally
            illuminate the bottom halves more. This is why in the indirectly lit
            scene, the bottom halves are brighter than the top halves, since
            the bottom halves have most of their light coming from indirect light
            while the top halves have most coming directly from the light. Another
            thing to notice is that in the indirectly lit scene, the sides of the
            spheres are tinted slightly blue and red. This is not present in the
            directly lit scene since there are actually no red or blue light
            sources. The reason it is present in the indirectly lit scene is the
            fact that the light bounces off the red or blue
            walls and then depositis some of this color on to the spheres.
          </p>


          <h4 align="middle">Various Max Ray Depths</h4>
          <p>
            Recall that the parameter max_ray_depth was essentially the number of
            times we allow a light ray to bounce and still affect the scene. Let
            us examine the effect that has on our final output. Below are images
            of the bunny dae all using 8 light rays and 16 samples per pixel,
            but with different max_ray_depth values.
          </p>
          <div align="center">
              <table style="width=100%">
                  <tr>
                      <td align="middle">
                          <img src="images/p4/bunny_m0.png" height="360px" />
                          <figcaption align="middle">Max Ray Depth: 0</figcaption>
                      </td>
                      <td align="middle">
                          <img src="images/p4/bunny_m1.png" height="360px" />
                          <figcaption align="middle">Max Ray Depth: 1</figcaption>
                      </td>
                      <td align="middle">
                          <img src="images/p4/bunny_m2.png" height="360px" />
                          <figcaption align="middle">Max Ray Depth: 2</figcaption>
                      </td>
                  </tr>
                  <tr>
                      <td align="middle">
                          <img src="images/p4/bunny_m3.png" height="360px" />
                          <figcaption align="middle">Max Ray Depth: 3</figcaption>
                      </td>
                      <!-- m=10 not required, only here for symmetry -->
                      <td align="middle">
                          <img src="images/p4/bunny_m10.png" height="360px" />
                          <figcaption align="middle">Max Ray Depth: 10</figcaption>
                      </td>
                      <td align="middle">
                          <img src="images/p4/bunny_m100.png" height="360px" />
                          <figcaption align="middle">Max Ray Depth: 100</figcaption>
                      </td>
                  </tr>
              </table>
          </div>
          <p>
            As expected we can see that as we allow more bounces, we see more
            color bleeding and less intense shadows. One thing to notice is that
            as we increase the number of allowed bounces however, the differences
            become less and less noticeable. This is what would be expected since
            as light bounces more and more its effect on the scene becomes less
            and less generally. This is why we constructed our Russian Roulette
            termination probability based on the amount the remaining light will
            effect the scene, which we estimated by using its illuminance.
          </p>

          <h4 align="middle">Various sample-per-pixel rates</h4>
          <p>
            Let us now examine how different sample-per-pixel rates effect the
            rendered scene. Below are images of the bunny dae using max ray depth
            of 5 and 4 light rays per pixel, but with various sample-per-pixel
            rates:
          </p>
          <div align="center">
              <table style="width=100%">
                  <tr>
                      <td align="middle">
                          <img src="images/p4/bunny_s1.png" height="360px" />
                          <figcaption align="middle">Samples: 1</figcaption>
                      </td>
                      <td align="middle">
                          <img src="images/p4/bunny_s2.png" height="360px" />
                          <figcaption align="middle">Samples: 2</figcaption>
                      </td>
                      <td align="middle">
                          <img src="images/p4/bunny_s4.png" height="360px" />
                          <figcaption align="middle">Samples: 4</figcaption>
                      </td>
                  </tr>
                  <tr>
                      <td align="middle">
                          <img src="images/p4/bunny_s8.png" height="360px" />
                          <figcaption align="middle">Samples: 8</figcaption>
                      </td>
                      <td align="middle">
                          <img src="images/p4/bunny_s16.png" height="360px" />
                          <figcaption align="middle">Samples: 16</figcaption>
                      </td>
                      <td align="middle">
                          <img src="images/p4/bunny_s64.png" height="360px" />
                          <figcaption align="middle">Samples: 64</figcaption>
                      </td>
                  </tr>
                  <tr>
                      <td align="middle" colspan="3">
                          <img src="images/p4/bunny_s1024.png" height="360px" />
                          <figcaption align="middle">Samples: 1024</figcaption>
                      </td>
                  </tr>
              </table>
          </div>
          <p>
            As we can see, as we increase our number of samples the noise begins
            to diminish. However, as the above images illustrate, the results
            do not really become bearable until we hit the really high sampling
            rates such as 1024 and 2048. Of course, sampling each pixel this many
            times becomes extremely expensive. In the next section, we will look
            at a way to achieve good results in a less expensive manner.
          </p>
      </p>

      <!-- ############################################################### -->
       <!-- ########################### PART 5 ########################### -->
       <!-- ############################################################### -->
      <h2 align="middle">Part 5: Adaptive Sampling</h2>

      <h3 align="middle">Implementation</h3>
      <p>
        As we saw in the above section, our current path tracing results in a
        lot of noise. In order to reduce the amount of noise, we saw that we
        could increase the number of samples per pixel. However, this can quickly
        become expensive. But do we have to sample each pixel the same amount
        of times? Wouldn't it be better if we knew which pixels needed more
        samples and which pixels didn't. This would allow us to achieve the results
        of using a lot of samples at each pixel, but save us on computation. This
        is known as Adaptive Sampling. It turns out that a good heuristic for
        estimating if we should continue sampling a pixel involves using the
        samples' illuminances' variance and mean. Our process for ray tracing
        for a pixel is modified as follows:
        <ul>
            <li> For each sample:</li>
            <ul>
                <li> Add the illuminance of this sample to a running sum s1 </li>
                <li> Add the illuminance squared to a running sum s2</li>
                <li>
                  Compute the mean and variance as follows:
                  <br>
                    <img src="images/p5/eq3.png" height="150px">
                  <br>
                  where n is the number of samples accumulated thus far.
                </li>
                <li>
                  Construct a variable I such that:
                  <br>
                    <img src="images/p5/eq1.png" height="75px">
                </li>
                <li>
                  Now we know from statistics that with a z-value of 1.96, if:
                  <br>
                    <img src="images/p5/eq2.png" height="50px">
                  <br>
                  where maxTolerance is a paramater we can tune, usually we used
                  0.05, that we are 95% confident that the average value is within
                  maxTolerance*mu of I. Thus, we assume the pixel has converged and stop tracing
                  if the above condition is true.
                </li>
            </ul>
        </ul>
        As we can see, this allows us a way of estimating when we should stop
        sampling from a pixel because the value has converged. We can now treat
        the given sample-per-pixel rate as a maximum number of samples, where we
        stop sampling if they pixel has converged or if we have reached the maximum
        number of samples.
      </p>
      <h2 align="middle">Examples</h2>
      <p>
        We can now render images using extremely high sample-per-pixel rates, relying
        on our adaptive sampling to limit the amount of sampling. Below are some
        examples of scenes rendered with the maximum number of samples set to a
        whopping 2048!
      </p>
      <div align="center">
          <table style="width=100%">
              <tr>
                  <td align="middle">
                      <img src="images/p5/bunny_s2048.png" height="360px" />
                  </td>
                  <td align="middle">
                      <img src="images/p5/spheres_s2048.png" height="360px" />
                  </td>
              </tr>
          </table>
      </div>
      <p>
        As we can see the amount of noise is drastically reduced! Let us analyze
        why by analyzing the sampling rates of each pixel used for the images.
        Below is a visualization of the sampling rates at each pixel, where
        red and blue colors represent high and low sampling rates respectively.
        The sampling rates are computed as the ratio between the actual number
        of samples used and the maximum number of samples allowed, which was
        2048 for the above images.
      </p>
      <div align="center">
          <table style="width=100%">
              <tr>
                  <td align="middle">
                      <img src="images/p5/bunny_s2048_rate.png" height="360px" />
                  </td>
                  <td align="middle">
                      <img src="images/p5/spheres_s2048_rate.png" height="360px" />
                  </td>
              </tr>
          </table>
      </div>
      <p>
        What this shows is that we saved a lot of time on the light, and a good
        amount of time on the walls. For these areas we sampled much less than
        2048 times per pixel. On the more detailed areas, however, such as the bunny's
        face and body, we required most or all of our maximum allowed number of
        samples.
      </p>
    </div>
</body>
</html>
