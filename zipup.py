#!/usr/bin/env python3

import glob
import os
import zipfile

DEFAULT_FILES = ['src/*.cpp', 'src/*.h', 'src/static_scene/*.cpp', 'src/static_scene/*.h', 'website/**/*']
SUBMISSION_URL = "https://okpy.org/cal/cs184/sp17/pathtracer/"
SUBMISSION_FILENAME = "{}_proj3_pathtracer.zip"


def print_yellow(message):
    print("\033[93m{}\033[0m".format(message))


def change_to_script_dir():
    """Changes the current working directory of the running script to that of
    the scriptfile. This allows us to find all the files we need to zip up.
    """
    script_path = os.path.abspath(__file__)
    script_dir = os.path.dirname(script_path)
    os.chdir(script_dir)


def main():
    change_to_script_dir()
    files_to_zip = []

    for file_glob in DEFAULT_FILES:
        files_to_zip.extend(glob.glob(file_glob))

    print("Zipping the following directories...")

    for f in files_to_zip:
        print("* {}".format(f))

    login = input('\nEnter your login for cs184.org: ')
    zipname = SUBMISSION_FILENAME.format(login)

    with zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for f in files_to_zip:
            zipf.write(f)

    print_yellow("\nCreated a new zip file '{}' in your project directory."
                 .format(zipname))
    print_yellow("You're not done yet!")
    print_yellow("Please visit {} to submit your assignment."
                 .format(SUBMISSION_URL))


if __name__ == "__main__":
    main()
